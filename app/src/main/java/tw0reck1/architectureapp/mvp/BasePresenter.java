package tw0reck1.architectureapp.mvp;

/**
 * Created by akra on 05.04.18.
 */
public interface BasePresenter<T extends BaseView> {

    void setView(T view);

}