package tw0reck1.architectureapp;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

import tw0reck1.architectureapp.di.DaggerAppComponent;

/**
 * Created by akra on 04.04.18.
 */
public class ArchApplication extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }

}