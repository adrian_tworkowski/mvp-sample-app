package tw0reck1.architectureapp.di;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

import tw0reck1.architectureapp.ArchApplication;
import tw0reck1.architectureapp.api.module.ApiModule;
import tw0reck1.architectureapp.data.RepositoryModule;
import tw0reck1.architectureapp.images.ImagesModule;

/**
 * Created by akra on 04.04.18.
 */
@Singleton
@Component(modules = {AppModule.class, RepositoryModule.class, ApiModule.class, ImagesModule.class, ActivityBindingModule.class, AndroidSupportInjectionModule.class})
public interface AppComponent extends AndroidInjector<ArchApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        AppComponent.Builder application(Application application);

        AppComponent build();

    }

}