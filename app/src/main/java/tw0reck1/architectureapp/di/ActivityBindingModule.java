package tw0reck1.architectureapp.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

import tw0reck1.architectureapp.di.scopes.ActivityScope;
import tw0reck1.architectureapp.orders.OrdersActivity;
import tw0reck1.architectureapp.orders.details.OrderDetailsModule;
import tw0reck1.architectureapp.orders.list.OrdersListModule;

@Module
public abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = {OrdersListModule.class, OrderDetailsModule.class})
    abstract OrdersActivity ordersActivity();

}