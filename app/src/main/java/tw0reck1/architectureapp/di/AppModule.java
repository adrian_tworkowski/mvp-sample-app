package tw0reck1.architectureapp.di;

import android.app.Application;
import android.content.Context;

import dagger.Binds;
import dagger.Module;

/**
 * Created by akra on 04.04.18.
 */
@Module
public abstract class AppModule {

    @Binds
    abstract Context bindContext(Application application);

}