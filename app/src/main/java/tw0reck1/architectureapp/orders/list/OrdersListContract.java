package tw0reck1.architectureapp.orders.list;

import java.util.Collection;

import tw0reck1.architectureapp.data.model.Order;
import tw0reck1.architectureapp.mvp.BasePresenter;
import tw0reck1.architectureapp.mvp.BaseView;

/**
 * Created by akra on 15.03.18.
 */
public class OrdersListContract {

    public interface View extends BaseView {

        public void showLoadingView(boolean show);

        public void showOrders(Collection<Order> orders);

        public void showError(String message);

        public void showOrderDetails(Order order);

    }

    public interface Presenter extends BasePresenter<View> {

        public void loadOrders();

        public void openOrderDetails(Order order);

    }

}