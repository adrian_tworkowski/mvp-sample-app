package tw0reck1.architectureapp.orders.list;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

import tw0reck1.architectureapp.di.scopes.ActivityScope;
import tw0reck1.architectureapp.di.scopes.FragmentScope;

/**
 * Created by akra on 15.03.18.
 */
@Module
public abstract class OrdersListModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract OrdersListFragment ordersFragment();

    @ActivityScope
    @Binds
    abstract OrdersListContract.Presenter ordersPresenter(OrdersListPresenter presenter);

}