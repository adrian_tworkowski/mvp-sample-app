package tw0reck1.architectureapp.orders.list;

import java.util.List;

import javax.inject.Inject;

import tw0reck1.architectureapp.data.OrdersDataSource;
import tw0reck1.architectureapp.data.OrdersRepository;
import tw0reck1.architectureapp.data.model.Order;

/**
 * Created by akra on 15.03.18.
 */
public class OrdersListPresenter implements OrdersListContract.Presenter {

    protected OrdersListContract.View mOrdersView;

    private final OrdersRepository mOrdersRepository;

    @Inject
    public OrdersListPresenter(OrdersRepository ordersRepository) {
        mOrdersRepository = ordersRepository;
    }

    @Override
    public void setView(OrdersListContract.View view) {
        mOrdersView = view;
    }

    @Override
    public void loadOrders() {
        mOrdersView.showLoadingView(true);

        mOrdersRepository.getOrders(new OrdersDataSource.GetOrdersCallback() {
            @Override
            public void onOrdersLoaded(List<Order> orders) {
                mOrdersView.showLoadingView(false);
                mOrdersView.showOrders(orders);
            }

            @Override
            public void onDataNotAvailable() {
                mOrdersView.showLoadingView(false);
                mOrdersView.showError("Data not available");
            }
        });
    }

    @Override
    public void openOrderDetails(Order order) {
        mOrdersView.showOrderDetails(order);
    }

}