package tw0reck1.architectureapp.orders.list;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import dagger.android.DaggerFragment;

import tw0reck1.architectureapp.R;
import tw0reck1.architectureapp.data.model.Order;
import tw0reck1.architectureapp.images.ImageLoader;
import tw0reck1.architectureapp.orders.OrdersActivity;
import tw0reck1.architectureapp.utils.OrderUtils;

import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;
import static tw0reck1.architectureapp.orders.details.OrderDetailsFragment.ORDER_ID;

/**
 * Created by akra on 04.04.18.
 */
public class OrdersListFragment extends DaggerFragment implements OrdersListContract.View {

    @Inject
    protected OrdersListContract.Presenter mPresenter;

    @Inject
    protected ImageLoader mImageLoader;

    private OrdersAdapter mListAdapter;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Inject
    public OrdersListFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mListAdapter = new OrdersAdapter(mPresenter, mImageLoader);

        mPresenter.setView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.orders_fragment, container, false);

        ListView listView = root.findViewById(R.id.listview_orders);
        listView.setAdapter(mListAdapter);

        mSwipeRefreshLayout = root.findViewById(R.id.swiperefresh_orders);
        mSwipeRefreshLayout.setOnRefreshListener(() -> mPresenter.loadOrders());

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mPresenter.loadOrders();
    }

    @Override
    public void showLoadingView(boolean show) {
        mSwipeRefreshLayout.setRefreshing(show);
    }

    @Override
    public void showOrders(Collection<Order> orders) {
        mListAdapter.setOrders(new ArrayList<>(orders));
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), "Test error " + message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showOrderDetails(Order order) {
        Intent intent = new Intent(getActivity(), OrdersActivity.class);
        intent.putExtra(ORDER_ID, order.getOrderId());
        intent.setFlags(FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private static class OrdersAdapter extends BaseAdapter implements Comparator<Order> {

        private final List<Order> mOrders = new ArrayList<>();

        private final OrdersListContract.Presenter mPresenter;
        private final ImageLoader mImageLoader;

        public OrdersAdapter(OrdersListContract.Presenter presenter, ImageLoader imageLoader) {
            mPresenter = presenter;
            mImageLoader = imageLoader;
        }

        public void setOrders(List<Order> orders) {
            mOrders.clear();
            mOrders.addAll(orders);

            Collections.sort(mOrders, this);

            notifyDataSetChanged();
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            final Order order = getItem(i);

            View rowView = view;
            if (rowView == null) {
                LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
                rowView = inflater.inflate(R.layout.order_row, viewGroup, false);

                rowView.setOnClickListener(v -> mPresenter.openOrderDetails(order));
            }

            TextView titleTV = rowView.findViewById(R.id.textview_title);
            titleTV.setText(order.getTitle());

            TextView descriptionTV = rowView.findViewById(R.id.textview_description);
            descriptionTV.setText(OrderUtils.getParsedDescription(order));

            TextView dateTV = rowView.findViewById(R.id.textview_date);
            dateTV.setText(OrderUtils.getParsedDate(order));

            ImageView imageIV = rowView.findViewById(R.id.imageview_image);
            mImageLoader.loadImage(imageIV, order.getImageUrl());

            return rowView;
        }

        @Override
        public int getCount() {
            return mOrders.size();
        }

        @Override
        public Order getItem(int i) {
            return mOrders.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public int compare(Order o1, Order o2) {
            return Long.compare(o1.getOrderId(), o2.getOrderId());
        }

    }

}