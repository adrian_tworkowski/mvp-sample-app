package tw0reck1.architectureapp.orders.details;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

import tw0reck1.architectureapp.di.scopes.ActivityScope;
import tw0reck1.architectureapp.di.scopes.FragmentScope;

/**
 * Created by akra on 06.04.18.
 */
@Module
public abstract class OrderDetailsModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract OrderDetailsFragment orderFragment();

    @ActivityScope
    @Binds
    abstract OrderDetailsContract.Presenter orderPresenter(OrderDetailsPresenter presenter);

}