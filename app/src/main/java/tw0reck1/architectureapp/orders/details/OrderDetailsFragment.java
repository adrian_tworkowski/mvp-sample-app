package tw0reck1.architectureapp.orders.details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import javax.inject.Inject;

import dagger.android.DaggerFragment;

import tw0reck1.architectureapp.R;
import tw0reck1.architectureapp.data.model.Order;
import tw0reck1.architectureapp.orders.OrdersActivity;
import tw0reck1.architectureapp.utils.OrderUtils;

/**
 * Created by akra on 06.04.18.
 */
public class OrderDetailsFragment extends DaggerFragment implements OrderDetailsContract.View {

    public static final String ORDER_ID = OrdersActivity.class.getSimpleName() + ".ORDER_ID";

    @Inject
    protected OrderDetailsContract.Presenter mPresenter;

    private WebView mWebView;

    @Inject
    public OrderDetailsFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter.setView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.order_details_fragment, container, false);

        mWebView = root.findViewById(R.id.webview);
        mWebView.setWebViewClient(new WebViewClient());

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        onViewReady();
    }

    @Override
    public void onViewReady() {
        mPresenter.loadOrder(getArguments().getLong(ORDER_ID));
    }

    @Override
    public void showOrderDetails(Order order) {
        mWebView.loadUrl(OrderUtils.getUrl(order));
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), "Test error " + message, Toast.LENGTH_LONG).show();
    }

}