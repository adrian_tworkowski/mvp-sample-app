package tw0reck1.architectureapp.orders.details;

import javax.inject.Inject;

import tw0reck1.architectureapp.data.OrdersDataSource;
import tw0reck1.architectureapp.data.OrdersRepository;
import tw0reck1.architectureapp.data.model.Order;

/**
 * Created by akra on 06.04.18.
 */
public class OrderDetailsPresenter implements OrderDetailsContract.Presenter {

    protected OrderDetailsContract.View mOrderView;

    private final OrdersRepository mOrdersRepository;

    @Inject
    public OrderDetailsPresenter(OrdersRepository ordersRepository) {
        mOrdersRepository = ordersRepository;
    }

    @Override
    public void setView(OrderDetailsContract.View view) {
        mOrderView = view;
    }

    @Override
    public void loadOrder(Long orderId) {
        mOrdersRepository.getOrder(orderId, new OrdersDataSource.GetOrderCallback() {
            @Override
            public void onOrderLoaded(Order order) {
                mOrderView.showOrderDetails(order);
            }

            @Override
            public void onDataNotAvailable() {
                mOrderView.showError("Data not available");
            }
        });
    }

}