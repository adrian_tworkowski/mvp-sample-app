package tw0reck1.architectureapp.orders.details;

import tw0reck1.architectureapp.data.model.Order;
import tw0reck1.architectureapp.mvp.BasePresenter;
import tw0reck1.architectureapp.mvp.BaseView;

/**
 * Created by akra on 06.04.18.
 */
public class OrderDetailsContract {

    public interface View extends BaseView {

        public void onViewReady();

        public void showOrderDetails(Order order);

        public void showError(String message);

    }

    public interface Presenter extends BasePresenter<View> {

        public void loadOrder(Long orderId);

    }

}