package tw0reck1.architectureapp.orders;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import dagger.Lazy;
import dagger.android.DaggerActivity;

import tw0reck1.architectureapp.R;
import tw0reck1.architectureapp.orders.details.OrderDetailsFragment;
import tw0reck1.architectureapp.orders.list.OrdersListFragment;

import static tw0reck1.architectureapp.orders.details.OrderDetailsFragment.ORDER_ID;

/**
 * Created by akra on 15.03.18.
 */
public class OrdersActivity extends DaggerActivity {

    @Inject
    Lazy<OrdersListFragment> mOrdersFragmentProvider;
    @Inject
    Lazy<OrderDetailsFragment> mOrderDetailsFragmentProvider;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orders_activity);

        Fragment ordersFragment = getFragmentManager().findFragmentById(R.id.container);

        if (ordersFragment == null) {
            ordersFragment = mOrdersFragmentProvider.get();

            getFragmentManager().beginTransaction()
                    .add(R.id.container, ordersFragment)
                    .commit();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Long orderId = intent.getLongExtra(ORDER_ID, -1);
        if (orderId == -1) return;

        Fragment fragment = null;

        Bundle bundle = new Bundle(1);
        bundle.putLong(ORDER_ID, orderId);

        if (findViewById(R.id.container_details) != null) {
            fragment = getFragmentManager().findFragmentById(R.id.container_details);

            if (fragment == null) {
                OrderDetailsFragment orderFragment = mOrderDetailsFragmentProvider.get();
                orderFragment.setArguments(bundle);

                getFragmentManager().beginTransaction()
                        .replace(R.id.container_details, orderFragment)
                        .commit();
            } else {
                fragment.setArguments(bundle);
                ((OrderDetailsFragment) fragment).onViewReady();
            }
        } else {
            fragment = getFragmentManager().findFragmentById(R.id.container);

            if (fragment == null || !(fragment instanceof OrderDetailsFragment)) {
                OrderDetailsFragment orderFragment = mOrderDetailsFragmentProvider.get();
                orderFragment.setArguments(bundle);

                getFragmentManager().beginTransaction()
                        .add(R.id.container, orderFragment).addToBackStack(null)
                        .commit();
            } else {
                fragment.setArguments(bundle);
                ((OrderDetailsFragment) fragment).onViewReady();
            }
        }
    }

}