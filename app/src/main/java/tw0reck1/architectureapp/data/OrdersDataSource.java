package tw0reck1.architectureapp.data;

import android.support.annotation.NonNull;

import java.util.List;

import tw0reck1.architectureapp.data.model.Order;

/**
 * Created by akra on 10.04.18.
 */
public interface OrdersDataSource {

    interface GetOrdersCallback {

        void onOrdersLoaded(List<Order> orders);

        void onDataNotAvailable();

    }

    interface GetOrderCallback {

        void onOrderLoaded(Order order);

        void onDataNotAvailable();

    }

    void getOrders(@NonNull GetOrdersCallback callback);

    void getOrder(@NonNull Long orderId, @NonNull GetOrderCallback callback);

    void saveOrders(@NonNull List<Order> orders);

}