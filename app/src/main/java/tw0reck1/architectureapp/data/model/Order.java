package tw0reck1.architectureapp.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by akra on 15.03.18.
 */
@Entity(tableName = "orders")
public class Order {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "orderId")
    @Expose
    @SerializedName("orderId")
    private Long mOrderId;

    @Nullable
    @ColumnInfo(name = "title")
    @Expose
    @SerializedName("title")
    private String mTitle;

    @Nullable
    @ColumnInfo(name = "description")
    @Expose
    @SerializedName("description")
    private String mDescription;

    @Nullable
    @ColumnInfo(name = "modificationDate")
    @Expose
    @SerializedName("modificationDate")
    private Date mModificationDate;

    @Nullable
    @ColumnInfo(name = "image_url")
    @Expose
    @SerializedName("image_url")
    private String mImageUrl;

    @Ignore
    public Order() {}

    @Ignore
    public Order(@NonNull Long orderId) {
        mOrderId = orderId;
    }

    public Order(@NonNull Long orderId, @Nullable String title, @Nullable String description,
                @Nullable Date modificationDate, @Nullable String imageUrl) {
        mOrderId = orderId;
        mTitle = title;
        mDescription = description;
        mModificationDate = modificationDate;
        mImageUrl = imageUrl;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public Long getOrderId() {
        return mOrderId;
    }

    public Date getModificationDate() {
        return mModificationDate;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    @Override
    public String toString() {
        return String.format("%d-%s", mOrderId, mTitle);
    }

}