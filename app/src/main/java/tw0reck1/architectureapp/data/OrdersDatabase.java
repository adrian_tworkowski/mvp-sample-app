package tw0reck1.architectureapp.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import tw0reck1.architectureapp.data.model.Order;

/**
 * Created by akra on 10.04.18.
 */
@TypeConverters({Converters.class})
@Database(entities = {Order.class}, version = 1)
public abstract class OrdersDatabase extends RoomDatabase {

    public abstract OrdersDao ordersDao();

}