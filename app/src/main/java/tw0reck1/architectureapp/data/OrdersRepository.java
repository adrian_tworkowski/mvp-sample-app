package tw0reck1.architectureapp.data;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import tw0reck1.architectureapp.api.ApiOrdersDataSource;
import tw0reck1.architectureapp.data.model.Order;

/**
 * Created by akra on 10.04.18.
 */
@Singleton
public class OrdersRepository implements OrdersDataSource {

    private final OrdersDataSource mLocalDataSource, mApiDataSource;

    @Inject
    public OrdersRepository(LocalOrdersDataSource localDataSource, ApiOrdersDataSource apiDataSource) {
        mLocalDataSource = localDataSource;
        mApiDataSource = apiDataSource;
    }

    @Override
    public void getOrders(@NonNull GetOrdersCallback callback) {
        mApiDataSource.getOrders(new GetOrdersCallback() {
            @Override
            public void onOrdersLoaded(List<Order> orders) {
                if (!orders.isEmpty()) {
                    mLocalDataSource.saveOrders(orders);

                    callback.onOrdersLoaded(orders);
                } else {
                    onDataNotAvailable();
                }
            }

            @Override
            public void onDataNotAvailable() {
                mLocalDataSource.getOrders(callback);
            }
        });
    }

    @Override
    public void getOrder(@NonNull Long orderId, @NonNull GetOrderCallback callback) {
        mApiDataSource.getOrder(orderId, new GetOrderCallback() {
            @Override
            public void onOrderLoaded(Order order) {
                if (order != null) {
                    callback.onOrderLoaded(order);
                } else {
                    onDataNotAvailable();
                }
            }

            @Override
            public void onDataNotAvailable() {
                mLocalDataSource.getOrder(orderId, callback);
            }
        });
    }

    @Override
    public void saveOrders(@NonNull List<Order> orders) {
        mApiDataSource.saveOrders(orders);
        mLocalDataSource.saveOrders(orders);
    }

}