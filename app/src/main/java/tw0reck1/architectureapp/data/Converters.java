package tw0reck1.architectureapp.data;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

/**
 * Created by akra on 10.04.18.
 */
public class Converters {

    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }

}