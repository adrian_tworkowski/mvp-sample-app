package tw0reck1.architectureapp.data;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Inject;
import javax.inject.Singleton;

import tw0reck1.architectureapp.data.model.Order;

/**
 * Created by akra on 10.04.18.
 */
@Singleton
public class LocalOrdersDataSource implements OrdersDataSource {

    private final OrdersDao mOrdersDao;

    private final Executor mExecutor;
    private final Handler mMainHandler;

    @Inject
    public LocalOrdersDataSource(OrdersDao ordersDao) {
        mOrdersDao = ordersDao;

        mMainHandler = new Handler(Looper.getMainLooper());
        mExecutor = Executors.newSingleThreadExecutor();
    }

    @Override
    public void getOrders(@NonNull GetOrdersCallback callback) {
        mExecutor.execute(() -> {
            List<Order> orders = mOrdersDao.getOrders();
            mMainHandler.post(() -> callback.onOrdersLoaded(orders));
        });
    }

    @Override
    public void getOrder(@NonNull Long orderId, @NonNull GetOrderCallback callback) {
        mExecutor.execute(() -> {
            Order order = mOrdersDao.getOrder(orderId) ;
            mMainHandler.post(() -> callback.onOrderLoaded(order));
        });
    }

    @Override
    public void saveOrders(@NonNull List<Order> orders) {
        mExecutor.execute(() -> mOrdersDao.insertOrders(orders));
    }

}