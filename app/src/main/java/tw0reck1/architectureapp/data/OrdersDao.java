package tw0reck1.architectureapp.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.Collection;
import java.util.List;

import tw0reck1.architectureapp.data.model.Order;

/**
 * Created by akra on 10.04.18.
 */
@Dao
public interface OrdersDao {

    @Query("SELECT * FROM orders")
    List<Order> getOrders();

    @Query("SELECT * FROM orders WHERE orderId = :orderId")
    Order getOrder(Long orderId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOrders(Collection<Order> orders);

}