package tw0reck1.architectureapp.data;

import android.app.Application;
import android.arch.persistence.room.Room;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by akra on 10.04.18.
 */
@Module
public abstract class RepositoryModule {

    @Singleton
    @Provides
    static OrdersDatabase provideDatabase(Application application) {
        return Room.databaseBuilder(application, OrdersDatabase.class, "orders.db").build();
    }

    @Singleton
    @Provides
    static OrdersDao provideTasksDao(OrdersDatabase database) {
        return database.ordersDao();
    }

}