package tw0reck1.architectureapp.images;

import android.widget.ImageView;

/**
 * Created by akra on 11.04.18.
 */
public interface ImageLoader {

    public void loadImage(ImageView view, String imageUrl);

}