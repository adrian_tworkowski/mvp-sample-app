package tw0reck1.architectureapp.images;

import android.widget.ImageView;

import com.bumptech.glide.Glide;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by akra on 11.04.18.
 */
@Singleton
public class GlideImageLoader implements ImageLoader {

    @Inject
    public GlideImageLoader() {}

    @Override
    public void loadImage(ImageView view, String imageUrl) {
        Glide.with(view).load(imageUrl).into(view);
    }

}