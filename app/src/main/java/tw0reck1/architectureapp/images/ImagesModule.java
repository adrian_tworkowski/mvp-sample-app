package tw0reck1.architectureapp.images;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

/**
 * Created by akra on 11.04.18.
 */
@Module
public abstract class ImagesModule {

    @Singleton
    @Binds
    abstract ImageLoader provideImageLoader(GlideImageLoader glideImageLoader);

}