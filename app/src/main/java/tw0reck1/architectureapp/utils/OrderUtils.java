package tw0reck1.architectureapp.utils;

import android.text.TextUtils;
import android.util.Patterns;

import java.text.DateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tw0reck1.architectureapp.data.model.Order;

/**
 * Created by akra on 10.04.18.
 */
public class OrderUtils {

    private OrderUtils() {}

    public static String getParsedDescription(Order order) {
        if (order == null || TextUtils.isEmpty(order.getDescription())) return null;

        return order.getDescription().replaceAll(Patterns.WEB_URL.pattern(), "");
    }

    public static String getParsedDate(Order order) {
        if (order == null || order.getModificationDate() == null) return null;

        return DateFormat.getDateInstance(DateFormat.LONG).format(order.getModificationDate());
    }

    public static String getUrl(Order order) {
        if (order == null || TextUtils.isEmpty(order.getDescription())) return null;

        Pattern pattern = Patterns.WEB_URL;
        Matcher matcher = pattern.matcher(order.getDescription());

        if (matcher.find()) return matcher.group();

        return null;
    }

}