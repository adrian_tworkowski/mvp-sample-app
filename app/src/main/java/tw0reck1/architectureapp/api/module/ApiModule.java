package tw0reck1.architectureapp.api.module;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dagger.Module;
import dagger.Provides;
import dagger.Reusable;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import tw0reck1.architectureapp.R;
import tw0reck1.architectureapp.api.ApiService;

/**
 * Created by akra on 15.03.18.
 */
@Module
public class ApiModule {

    @Provides
    @Reusable
    protected String provideBaseUrl(Context context) {
        return context.getString(R.string.api_base_url);
    }

    @Provides
    @Reusable
    protected ApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @Reusable
    protected Retrofit provideRetrofitClient(String baseUrl, GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    @Provides
    @Reusable
    protected GsonConverterFactory provideConverterFactory(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    @Reusable
    protected Gson provideGson() {
        return new GsonBuilder().create();
    }

}