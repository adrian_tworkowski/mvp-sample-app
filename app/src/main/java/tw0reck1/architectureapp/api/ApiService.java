package tw0reck1.architectureapp.api;

import retrofit2.Call;
import retrofit2.http.GET;

import tw0reck1.architectureapp.api.model.OrdersResponse;

/**
 * Created by akra on 15.03.18.
 */
public interface ApiService {

    @GET("~dpaluch/test35/")
    public Call<OrdersResponse> getOrders();

}