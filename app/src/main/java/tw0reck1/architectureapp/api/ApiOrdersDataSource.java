package tw0reck1.architectureapp.api;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import tw0reck1.architectureapp.api.model.OrdersResponse;
import tw0reck1.architectureapp.data.OrdersDataSource;
import tw0reck1.architectureapp.data.model.Order;

/**
 * Created by akra on 10.04.18.
 */
@Singleton
public class ApiOrdersDataSource implements OrdersDataSource {

    private final ApiService mApiService;

    @Inject
    public ApiOrdersDataSource(ApiService apiService) {
        mApiService = apiService;
    }

    @Override
    public void getOrders(@NonNull GetOrdersCallback callback) {
        Call<OrdersResponse> call = mApiService.getOrders();
        call.enqueue(new Callback<OrdersResponse>() {
            @Override
            public void onResponse(Call<OrdersResponse> call, Response<OrdersResponse> response) {
                OrdersResponse responseBody = response.body();

                List<Order> orders = (responseBody != null) ? responseBody.getOrders()
                        : new ArrayList<>();

                callback.onOrdersLoaded(orders);
            }

            @Override
            public void onFailure(Call<OrdersResponse> call, Throwable t) {
                callback.onDataNotAvailable();
            }
        });

    }

    @Override
    public void getOrder(@NonNull Long orderId, @NonNull GetOrderCallback callback) {
        callback.onDataNotAvailable();
    }

    @Override
    public void saveOrders(@NonNull List<Order> orders) {}

}