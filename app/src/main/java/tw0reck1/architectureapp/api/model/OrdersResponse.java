package tw0reck1.architectureapp.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import tw0reck1.architectureapp.data.model.Order;

/**
 * Created by akra on 15.03.18.
 */
public class OrdersResponse {

    @Expose
    @SerializedName("data")
    private List<Order> mOrders;

    public List<Order> getOrders() {
        return mOrders;
    }

}